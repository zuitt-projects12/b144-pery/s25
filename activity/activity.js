// Count fruits per country
db.fruits.aggregate(
	[
		{ $unwind: "$origin" },
		{ $group: { _id: "$origin", fruits: { $sum: 1 }} }
	]
)

// Count available fruits
db.fruits.aggregate(
	[
		{ $match: { onSale:true } },
		{ $count: "available_fruits" }
	]
)

// Count fruits with more than 20 stock
db.fruits.aggregate(
	[
		{ $match: { stock: { $gte: 20 } } },
		{ $count: "fruits_with_more_than_20_units" }
	]
)

// Get average price by supplier
db.fruits.aggregate(
	[
		{ $match: { onSale:true } },
		{ $group: { _id: "$supplier_id", avgPrice: { $avg: "$price" } } },
		{ $sort: { _id: -1 } }
		
	]
)

// Get highest price per supplier
db.fruits.aggregate(
	[
		{ $group: { _id: "$supplier_id", highestPrice: { $max: "$price" } } }
		
	]
)

// Get lowest price per supplier
db.fruits.aggregate(
	[
		{ $group: { _id: "$supplier_id", lowestPrice: { $min: "$price" } } }
		
	]
)