/* syntax for aggregate */

// db.fruits.aggregate(
//     [
//         {$match: {field: value}, ... }, ...
//         {$group: {_id: "$value", fieldResult: "$valueResult"}},
//         {$project: {fieldValues}}
//     ]
// )
        
// db.fruits.aggregate(
//     [
//         { $match: {onSale: true} },
//         { $group: {_id: "$supplier_id",  result: { $sum: "$stock" } }},
//         { $project: {_id: 0}}
//     ]
// )

// $project
// db.fruits.aggregate(
//     [
//         { $match: { onSale: true } },
//         { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
//         { $project: { _id: 0 } }
//     ]
// )

// counting all documents
// db.fruits.aggregate(
//     [
//         { $group: { _id: null, myCount: { $sum: 1 } } },
//         { $project: { _id: 0 } }
//     ]
// )

// sorting aggregated results
// db.fruits.aggregate(
//     [
//         { $match: { onSale: true } },
//         { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
//         { $sort: { total: 1 } }
//     ]
// )

// reverse order
// db.fruits.aggregate(
//     [
//         { $match: { onSale: true } },
//         { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
//         { $sort: { total: -1 } }
//     ]
// )
        
// aggregate arrays
// db.fruits.aggregate(
//     [
//         { $unwind: "$origin" },
//         { $group: { _id: "$origin", kinds: { $sum: 1 } } }
//     ]
// )
        
// db.fruits.aggregate(
//     [
//         { $unwind: "$color" },
//         { $group: { _id: "$color", kinds: { $sum: 1 } } }
//     ]
// )

// $count
db.scores.aggregate(
    [
        { $match: { score: { $gte: 80 } } },
        { $count: "passing_score" }
    ]
)